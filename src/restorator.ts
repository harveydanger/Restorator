/*
    Restorator - DOM Input Element storage and synchroniser
    Copyright (C) 2019 Vladimir Sukhov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


declare var document: Document;


type UploadFunction = (jsonString: string) => void;
type CallbackFunction = UploadFunction;
type DownloadFunction = () => Promise<string>;
type ModifierFunction = (element : RElement) => RElement;


/**
 * RElement - data structure of a watchable element
 * 
 * @param type string - DOMElement TagType
 * @param id string - DOMElement `id` attribute
 * @param name string - DOMElement `name` attribute
 * @param value string - DOMElement actual value | `value` attribute
 */

export type RElement ={
    type: string,
    id: string | null,
    name: string | null,
    value: string | null
}

/**
 * Store - data structure for element syncronisation
 * 
 * @param lastInstance string - Unix nano timestamp
 * @param elements RElement - @see RElement
 *      
 */
interface Store {
    lastInstance: string;
    elements: Array < RElement > ;
}

/**
 * Repeat - common interface for setting up reccuring call of Restorator
 * 
 * @param repeat boolean - set to yes if debug messages needed
 * @param delay number - interval between restorator sync calls
 */
interface Repeat {
    repeat: boolean,
    delay: number
}

/**
 * RestoratorOptions - Optional parameter to setup Restorator runtime
 * @see Restorator constructor method
 * 
 * @param name string - unique key that will be used for storage identification. @default GenerateName()
 * @param saveLocal boolean - identifies whether the restorator data to be saved in localStorage @default true
 * @param saveRemote boolean - identifies whether the restorator data to be synchronised with 3rd party server @default false
 * @param applicable [string] - list of element tag names that will be watched for.  
 *      @default ['input', 'textarea', 'select', 'checkbox']
 *      @throws Exception if item in applicable options is not in ['input', 'textarea', 'select']. Consider using onComplete instead.
 * @param onComplete Function: (Restorator)=>{} - invoked on every success operation of synchronisation. @optional
 * @param ignoreIds: [string] - list of HTML element Ids to be ignored @optional
 * @param debug: {use: boolean, highlight: boolean} - use for info and warning debug messages.
 */

export  interface RestoratorOptions {
    name: string;
    saveLocal: boolean;
    saveRemote: boolean;
    applicable: Array < string > ;
    onComplete?: CallbackFunction;
    ignoreIds ? : Array < string > ;
    debug ? : {
        use: boolean,
        highlight: boolean
    };
}

/**
 * Restorator @see README.md for details
 * 
 * @property options RestoratorOptions @see RestoratorOptions
 * @property elements [HTMLElement] - Array of actual DOM elements.
 * @property store Store @see Store
 * 
 * @readonly @property defaultDelay number - default delay between auto synchronisations, if not specified in Repeat options
 * @readonly @property cApplicable [string] - default supported HTML tags
 */

export class Restorator {

    options: RestoratorOptions; 
    elements: Array < HTMLElement > ; 
    toStore: Store; 

    readonly defaultDelay: number = 10000;  
    readonly cApplicable: Array<string> = ['input', 'textarea', 'select', 'checkbox', 'radio'];

    /**
     * @public SECTION
     */

    public constructor(options ? : RestoratorOptions) {
        this.elements = new Array < HTMLElement > ();
        this.toStore = this.WipeVirtualStore();

        if (!options) {
            this.options = { // setting default options
                name: this.GenerateName(),
                applicable: this.cApplicable,
                saveLocal: true,
                saveRemote: false,
            }
        } else { 
            if(options.applicable){
                options.applicable.forEach(e =>{
                    if(this.cApplicable.indexOf(e) === -1){
                        /**
                         * @warning
                         * @throws Exception on invalid tab types 
                         * */
                        throw "Restorator: Invalid options, restorator cannot watch for " + e
                    }
                })
            }
            this.options = options;
        }

        if (this.options.saveLocal) {
            if (!this.LSIsAvailable()) {
                console.error('Restorator: LocalStorage is not available. Discontinuing utilisation')
                this.options.saveLocal = false;
            }
        }

        if (this.options.debug) {
            if (this.options.debug.use) {
                if (!this.options.saveLocal && !this.options.saveRemote) {
                    console.warn('Restorator: Data only saved virtually, page refresh will destroy it');
                }
            }
        }
    }

    /**
     * GetCurStateAsJSONString
     * @public
     * 
     * @return string - JSON string of a current Restorator snapshot
     */
    public GetCurStateAsJSONString(): string {
        return JSON.stringify(this.toStore);
    }

    /**
     * GetCurStateAsStore
     * @public
     * @see Store for interface details
     * 
     * @return Store - current Restorator snapshot
     */
    public GetCurStateAsStore(): Store {
        return this.toStore;
    }

    /**
     * Sync - Synchronises virtual | local | server storages by taking the most recent data and filling in DOM elements' values
     * @public
     * @async
     * 
     * @param repeat Repeat @see Repeat
     * @param u UploadFunction @see UploadFunction
     * @param d DownloadFunction @see DownloadFunction
     * 
     * @returns Promise<void>
     */
    public async Sync(repeat?: Repeat, u ? : UploadFunction, d ? : DownloadFunction) : Promise<void> {
        if (this.options.debug) {
            if (this.options.debug.use) {
                console.info('Restorator: synchronising')
            }
        }

        let serverData: Store | null = null;
        
        if (this.options.saveRemote && this.toStore.lastInstance === "-1") {
            if (d) {
                if (this.options.debug) {
                    if (this.options.debug.use) {
                        console.info('Restorator: Downloading snapshot from server')
                    }
                }

                serverData = await this.DownloadFromServer(d);
                
                if(serverData){
                    if (!serverData.hasOwnProperty("lastInstance")){
                        if (this.options.debug) {
                            if (this.options.debug.use) {
                                console.log(serverData);
                                console.info('Restorator: Server does not seem to have recent data snapshot')
                            }
                        }
                        serverData = null;
                    }
                }
            } else {
                console.error("Restorator: Download function must be specified when server sync is used")
                return;
            }
        }

        let localStorageData: Store | null = null;

        if (this.options.saveLocal && this.toStore.lastInstance === "-1") {
            localStorageData = this.GetFromLocal(this.options.name)
        }

        if (this.toStore.lastInstance === "-1") { // initial synchronisation on first call
            if (serverData !== null && localStorageData !== null) {
                if (serverData.lastInstance <= localStorageData.lastInstance) {
                    this.toStore = JSON.parse(JSON.stringify(localStorageData));
                } else if (serverData.lastInstance > localStorageData.lastInstance) {
                    this.toStore = JSON.parse(JSON.stringify(serverData))
                }
                this.UpdateValues()
            } else if (serverData !== null && localStorageData === null) {
                if (this.options.debug) {
                    if (this.options.debug.use) {
                        console.info('Restorator: Using server storage as a source')
                    }
                }
                this.toStore = JSON.parse(JSON.stringify(serverData))
                this.UpdateValues()
            } else if (localStorageData !== null && serverData === null) {
                if (this.options.debug) {
                    if (this.options.debug.use) {
                        console.info('Restorator: Using local storage as a source')
                    }
                }
                this.toStore = JSON.parse(JSON.stringify(localStorageData));
                this.UpdateValues()
            } else {
                if (this.options.debug) {
                    if (this.options.debug.use) {
                        console.info('Restorator: No previous states found, creating...')
                    }
                }
                this.FindElements().BuildStore()
            }
        }else{
            this.toStore = this.WipeVirtualStore()
            this.FindElements().BuildStore()
        }

        if (this.options.saveLocal) {
            if (this.options.debug) {
                if (this.options.debug.use) {
                    console.info('Restorator: Saving to local storage')
                }
            }
            this.SaveLocal()
        }
        if (this.options.saveRemote) {
            if (u) {
                if (this.options.debug) {
                    if (this.options.debug.use) {
                        console.info('Restorator: Saving to server')
                    }
                }
                this.UploadToServer(u);
            }
        }

        if (repeat !== null && repeat !== undefined) {
            if (this.options.debug) {
                if (this.options.debug.use) {
                    console.info('Restorator: Setting up interval of: ' + (repeat.delay ? repeat.delay : this.defaultDelay) + ' for synchronisation')
                }
            }
            if (repeat.repeat) {
                setInterval(() => {
                    this.Sync(undefined, u, d)
                }, repeat.delay ? repeat.delay : this.defaultDelay)
            }
        }

        this.FindElements().BuildStore()
        if(this.options.onComplete){
            this.options.onComplete(JSON.stringify(this.toStore))
        }
    }

    /**
     * @private SECTION
     */

    /**
     * FindElements - Searches the document for `applicable` fields, filtering invalid and ignored
     * @fullfills this.elements
     * @private
     * 
     * @return Restorator - for method chaining
     */
    private FindElements(): Restorator {
        for (let i = 0; i < this.options.applicable.length; i++) {
            let elements : HTMLCollectionOf<Element> = document.getElementsByTagName(this.options.applicable[i]);
            let x: Array<HTMLElement> =[]
            
            for(let j = 0; j < elements.length; j++){
                x.push(<HTMLElement>elements.item(j))
            }
            
            this.elements = this.elements.concat((x.filter((e: HTMLElement) => {                
                if (e.id === "" && e.getAttribute('name') === "") {
                    if (this.options.debug) {
                        if (this.options.debug.use) {
                            console.warn('Restorator: Element found, but no name or id is assigned, skipping' + (this.options.debug.highlight ? '; highlighting in red' : ''), e);
                        }
                        if (this.options.debug.highlight) {
                            (<HTMLElement>e).style.backgroundColor = "red";
                        }
                    }
                    return false;
                } else if (this.options.ignoreIds) {
                    return this.options.ignoreIds.indexOf(e.id) === -1;
                }
                return true;
            })));
        }
        
        return this;
    }

    /**
     * BuildStore - Creates virtual storage of all collected inputs
     * @fullfills this.toStore
     * @private
     * 
     * @return Restorator - for method chaining
     */
    private BuildStore(): Restorator {
        this.toStore.lastInstance = this.GetCurrentTimeStamp()
        this.elements.forEach(elem => {  
            if(this.options.applicable.indexOf('checkbox') !== -1 && elem instanceof HTMLInputElement && elem.type === 'checkbox'){
                this.toStore.elements.push({
                    type: elem.tagName,
                    id: elem.id,
                    name: elem.getAttribute('name'),
                    value: elem.checked ? "checked" : "clear"
                })    
            } else if(this.options.applicable.indexOf('radio') !== -1 && elem instanceof HTMLInputElement && elem.type === 'radio'){
                this.toStore.elements.push({
                    type: elem.tagName,
                    id: elem.id,
                    name: elem.getAttribute('name'), // radio group
                    value: elem.checked ? "checked" : "clear"
                })    
            }else{
                this.toStore.elements.push({
                    type: elem.tagName,
                    id: elem.id,
                    name: elem.getAttribute('name'),
                    value: (<HTMLInputElement>elem).value
                })
            }
        })
        this.elements = new Array<HTMLElement>();
        return this;
    }

    /**
     * SaveLocal - Stores Restorator data in LocalStorage as JSON string
     * @private
     * 
     * @return Restorator - for method chaining
     */
    private SaveLocal(): Restorator {
        if (this.options.saveLocal) {
            localStorage.setItem(this.options.name, JSON.stringify(this.toStore));
        }
        return this;
    }

    /**
     * GetFromLocal - Obtain a copy of Restorator data from Local Storage
     * @private
     * 
     * @param key string - this.options.name as a key. 
     * 
     * @returns Store | null
     */
    private GetFromLocal(key: string): Store | null {
        let snapshot = localStorage.getItem(key)
        if (snapshot) {
            return JSON.parse(snapshot);
        } else {
            return null;
        }

    }

    /**
     * UploadToServer - Passes current Restorator JSON string storage to a user defined function for further transmission
     * @private
     * 
     * @param u UploadFunction @see UploadFunction
     * 
     * @return void
     */
    private UploadToServer(u: UploadFunction): void {
        u(JSON.stringify(this.toStore));
    }

    /**
     * DownloadFromServer - provides the interface for user function that downloads Restorator storage string
     * @private 
     * 
     * @param d DownloadFunction @see DownloadFunction
     * 
     * @return Promise<Store | null>
     */
    private DownloadFromServer(d: DownloadFunction): Promise<Store | null> {
        return new Promise((resolve, reject) => {
            d().then(async snapshot =>{
                if (snapshot) {
                    if (this.options.debug) {
                        if (this.options.debug.use) {
                            console.info('Restorator: Server download successful')
                        }
                    }
                    return resolve(JSON.parse(snapshot));
                } else {
                    if (this.options.debug) {
                        if (this.options.debug.use) {
                            console.warn('Restorator: Server download failed');
                        }
                    }
                    return resolve(null);
                }
            }).catch(e =>{
                console.error('Restorator: Server download failed');
                return null;
            }); 
        });   
    }

    /**
     * UpdateValues - Update DOM element values according to the Restorator data
     * @private
     * @async
     * 
     * @param modifier ModifierFunction @see ModifierFunction
     * 
     * @return Promise <void>
     */
    private async UpdateValues(modifier : ModifierFunction | null = null) : Promise<void>{
        if (this.options.debug) {
            if (this.options.debug.use) {
                console.info('Restorator: Updating HTML elements');
            }
        }
        this.toStore.elements.forEach(async (e : RElement) =>{
            var x;
            let ec : RElement = JSON.parse(JSON.stringify(e))
            
            if(modifier){
                ec = await modifier(ec)
            }

            if(ec.id !== "" && ec.id !== null){
                x = <HTMLInputElement> await document.getElementById(ec.id);   
            }else if(ec.name !== "" && ec.name !== null){
                x = <HTMLInputElement> await document.getElementsByName(ec.name)[0];
            }else{
                if (this.options.debug) {
                    if (this.options.debug.use) {
                        console.info('Restorator: Could not match element with its id and/or name',ec);
                    }
                }
            }

            if(x && ec.value && x.tagName === ec.type){
                if(this.options.applicable.indexOf('checkbox') !== -1 && x instanceof HTMLInputElement && x.type === 'checkbox'){
                    if(ec.value === "checked"){
                        x.checked = true;
                    }else{
                        x.checked = false;
                    } 
                } else if(this.options.applicable.indexOf('radio') !== -1 && x instanceof HTMLInputElement && x.type === 'radio'){
                    if(ec.value === "checked"){
                        x.checked = true;
                    }else{
                        x.checked = false;
                    } 
                }
                if(x.type !== 'radio'){ // Radio buttons must not change their values
                    x.value = ec.value;
                }
            }else{
                if(!ec.value){
                    if (this.options.debug) {
                        if (this.options.debug.use) {
                            console.info(ec.id + ' did not have value, skipping')
                        }
                    } 
                }else{
                    if (this.options.debug) {
                        if (this.options.debug.use) {
                            console.warn('Restorator: HTML element value:',x ? x.value : "NOT FOUND");
                            console.warn('Restorator: Restorator element value:',ec.value);
                            console.warn('Restorator: HTML element type: ', (!x ? "null" : x.tagName), " Restorator type: ", ec.type);
                            console.error('Restorator: Could not match element with its id',ec);
                        }
                    }
                }
            }
        });
    }

    /**
     * GenerateName - Generates random identified
     * @private
     * 
     * @param prefix - name prefix to be inserted before random string
     * 
     * @return string 
     */
    private GenerateName(prefix: string = "restorator-"): string {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        
        for (let i = 0; i < 5; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        return prefix + result;
    }

    /**
     * GetCurrentTimeStamp - Obtain current UNIX nano timestamp value
     * @private
     * 
     * @returns string
     */
    private GetCurrentTimeStamp(): string {
        return (new Date()).getTime().toString();
    }

    /**
     * LSIsAvailable - Checks if the LocalStorage currently available
     * @private
     * 
     * @returns boolean
     */
    private LSIsAvailable(): boolean {
        let t = 't';
        try {
            localStorage.setItem(t, t);
            localStorage.removeItem(t);
            return true;
        } catch (e) {
            return false;
        }
    }

    /**
     * WipeVirtualStore - Reinit virtual Restorator snapshot for synchronisation
     * @private
     * 
     * @returns Store - @see Store
     */
    private WipeVirtualStore() : Store{
        this.elements = new Array<HTMLElement>();
        return {
            lastInstance: "-1",
            elements: new Array < {
                type: "",
                id: "",
                name: "",
                value: ""
            } > ()
        };
    }
}