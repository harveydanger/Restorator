# Changes to the RestoratorJS project

### v1.0.2
- Radio support

### v1.0.1
- Checkbox support

### v1.0.0 
- Initial Release