# RestoratorJS

Restorator is a DOM Automatic Synchronisation ecosystem. This module handles synchronisation of user input into DOM Input elements within 3 different storages.
By using Restorator in your web application, you avoid having save buttons on pages and forms.

1. Virtual from DOM storage
2. Local Storage
3. Server Storage

If used on document load, restorator automatically obtains the recent version of a data snapshot from a local storage or a server (which is more recent) and inserts values to the fileds.

## Installation

```
npm install -g restoratorjs
```

## How to use

### Instantiation signature
```
    var r = new Restorator({
            name: "test", 
            applicable:['input', 'textarea', 'select'], 
            ignoreIds: [''], 
            debug: {use: true, highlight: true}, 
            saveLocal: true, 
            saveRemote: true, 
            onComplete: (json)=>{}
    })
```

### Parameter Description
- **name** string - unique key that will be used for storage identification. @default GenerateName()
- **saveLocal** boolean - identifies whether the restorator data to be saved in localStorage. Default true
- **saveRemote** boolean - identifies whether the restorator data to be synchronised with 3rd party server. Default false
- **applicable** [string] - list of element tag names that will be watched for. Default ['input', 'textarea', 'select', 'checkbox']
- **onComplete** Function: (json)=>{} - hook - invoked on every success operation of synchronisation. *Optional*
- **ignoreIds**: [string] - list of HTML element Ids to be ignored. *Optional*
- **debug**: {use: boolean, highlight: boolean} - use for info and warning debug messages. *Optional*

### How to synchronise DOM and Restorator

Simply call method `Sync(repeat, uFunc, dFunc)` on restorator instance.
Where: 

- **repeat** object - { repeat: true, delay: 1000 }, which will invoke syncronisation once a 1000 milliseconds
- **uFunc** function - (jsonString)=>{}, server upload function
- **dFunc** function - ()=>{return jsonString}, server download function that must return json string of restorator snapshot



***Copyright 2019 Vladimir Sukhov, GPLv3-higher***